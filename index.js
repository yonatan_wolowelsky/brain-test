var fs = require('fs');
var express = require('express');
var socket = require('socket.io');
var _ = require('underscore');
var csv = require('csv');

var app = express();
var server = app.listen(8787);
var io = socket.listen(server);
console.log("Serving at port 8787");

app.get('/', function(request, response){
  response.sendfile(__dirname + "/index.html");
});

var stream = fs.createReadStream("headband.csv");
var csvStream = csv.parse();
var delayTime = 1000 / 500;
var allData = [];
var cursor = 0;
var isFinishedParsing = false;
stream.pipe(csvStream);
csvStream.on("data", function (data) {
  allData.push(data);
}).on('end', function () {
  isFinishedParsing = true;
});

var emitData = function () {
  _.delay(function () {
    if (isFinishedParsing) {
      io.sockets.emit('data', allData[cursor]);
      cursor ++;
      if (cursor === allData.length) {
        cursor = 0;
      }
    }
    emitData();
  }, 100)
};

io.sockets.on('connection', function(socket) {
  emitData();
});
